 @performance
Feature: Check all pages
Scenario: Check page: licenta.local/home

Given http status for page "licenta.local/home" is 200
Given response time for page "licenta.local/home" is 0.298469
Given I am on "/home" 
Then I should not see the error message containing 'An error occurred'

Scenario: Check page: licenta.local/about_us
Given http status for page "licenta.local/about_us" is 200
Given response time for page "licenta.local/about_us" is 0.333275
Given I am on "/about_us" 
Then I should not see the error message containing 'An error occurred'

Scenario: Check page: licenta.local/our_team
Given http status for page "licenta.local/our_team" is 200
Given response time for page "licenta.local/our_team" is 0.191425
Given I am on "/our_team" 
Then I should not see the error message containing 'An error occurred'

Scenario: Check page: licenta.local/contact
Given http status for page "licenta.local/contact" is 200
Given response time for page "licenta.local/contact" is 0.083369
Given I am on "/contact" 
Then I should not see the error message containing 'An error occurred'
