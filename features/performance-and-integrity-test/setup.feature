@setup
Feature: Site Setup Report

Scenario: Check Site Database and Bootstrap
  Given I run drush status
  Then drush output should contain ':  mysql'
  Then drush output should contain ':  Successful'

Scenario: Get pages list from site
  Given I have pages list
  Then check pages